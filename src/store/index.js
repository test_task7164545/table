import Vue from 'vue'
import Vuex from 'vuex'
import data from '@/data/data'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    filterData: {
      form_errors: null,
      success_message: "Success",
      page_data: {
        data: [
          {
            iso_3166_1_a2: "RU",
            iso_3166_1_a3: "RUS",
            iso_3166_1_numeric: "643",
            printable_name: "Russian Federation",
            name: "",
            display_order: 0,
            is_shipping_country: false
          }
        ],
        page: {
          has_next: false,
          has_previous: false,
          has_other_pages: false,
          next_page_number: null,
          previous_page_number: null,
          start_index: 1,
          end_index: 1,
          total_count: 249,
          selected_count: 1,
          pages: 1
        }
      },
      redirect: "",
      exc_stack: "",
      debug: [],
      user_groups: "",
      user_perms: ""
    }
  },
  getters: {
    countriesData() {
      return data
    }
  },
  mutations: {
  },

  actions: {
  },

})
